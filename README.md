# Synthetic Basketball Scene

## About this project
* Unity Version: 2019.4.21


## Operation


## Scripts
### Camera
* **PTZCameraController:** Controll the rotation of PTZ cameras, including discrete rotation and continuous rotation.
* **MultiCameraController:** Switch between different PTZ cameras. Press the switch in UI to change the mode.
    * MODE1 (2 cameras)
        * LowPTZCamera
        * HighPTZCamera  
    * MODE2 (4 cameras)
        * SideCamera_lefttop
        * SideCamera_leftbottom
        * SideCamera_righttop
        * SideCamera_rightbottom 
* **CaptureCameraView:** Capture camera view in `OnPostRender()`. It excludes the UI canvas.

### UI
* **ButtonController:** Deal with every button event, including



### TCGM (Task Content Generation Module)
* LookAtUser: For each canvas above the virtual player, let it always can face the user.
* OPlayerController: The handler for each opponent virtual player includes (1) Initialize the position (2) Set the next position while running with trajectory. (3) Set the animation each idle or running.
* TaskManager: Set up the task content according to the configuration selected by the user. It includes (1) Setup for each question and (2) Pushing the state in the training such as controlling the virtual player to execute the next position.
* TimerManager: The handler for countdown.
* TPlayerController: The handler for each teammate virtual player includes (1) Initialize the position (2) Set the next position while running with trajectory. (3) Set the animation each idle or running.
* TrajectoryManager: The handler for trajectory includes (1) Loading and parsing trajectory from Resource Folder. (2) Generating the trajectory of the defender. (3) Getting the position for each virtual player at runtime.

### Others
* ButtonController: 
* CorrectManager: Check the answer to which the user response.

## Demo

