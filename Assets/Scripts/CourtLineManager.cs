﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CourtLineManager : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform edge_pt1;
    public Transform edge_pt2;
    public Transform edge_pt3;
    public Transform edge_pt4;

    public Transform penalty_l_pt1;
    public Transform penalty_l_pt2;
    public Transform penalty_l_pt3;
    public Transform penalty_l_pt4;

    public Transform penalty_r_pt1;
    public Transform penalty_r_pt2;
    public Transform penalty_r_pt3;
    public Transform penalty_r_pt4;

    public Transform middle_pt1;
    public Transform middle_pt2;

    public Camera cam;
    public MultiCameraController cameraController;

    void Start()
    {
        cam = cameraController.currentCamera;
    }

    // Update is called once per frame
    public string get_string_courtpoint_position(){
        Vector2[] points = new Vector2[14];
        string return_string = "";
        points[0] = cam.WorldToScreenPoint(edge_pt1.position);
        points[1] = cam.WorldToScreenPoint(edge_pt2.position);
        points[2] = cam.WorldToScreenPoint(edge_pt3.position);
        points[3] = cam.WorldToScreenPoint(edge_pt4.position);

        points[4] = cam.WorldToScreenPoint(penalty_l_pt1.position);
        points[5] = cam.WorldToScreenPoint(penalty_l_pt2.position);
        points[6] = cam.WorldToScreenPoint(penalty_l_pt3.position);
        points[7] = cam.WorldToScreenPoint(penalty_l_pt4.position);

        points[8] = cam.WorldToScreenPoint(penalty_r_pt1.position);
        points[9] = cam.WorldToScreenPoint(penalty_r_pt2.position);
        points[10] = cam.WorldToScreenPoint(penalty_r_pt3.position);
        points[11] = cam.WorldToScreenPoint(penalty_r_pt4.position);

        points[12] = cam.WorldToScreenPoint(middle_pt1.position);
        points[13] = cam.WorldToScreenPoint(middle_pt2.position);

        for(var i = 0;i<14;i++)
        {
            if(i == 0)
                return_string+=points[i].ToString().Replace("(","").Replace(")","").Replace(" ","");
            else
                return_string+='/'+points[i].ToString().Replace("(","").Replace(")","").Replace(" ","");
        }
        print(return_string);
        return (return_string);
    }

    void Update()
    {
       
    }
}
