﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewTaskManager : MonoBehaviour
{
    // Start is called before the first frame update
    public NewTrajectoryManager NewTrajectoryManager;
    public bool taskIsStart;
    public GameObject T1, T2, T3, T4,T5;
    public GameObject O1, O2, O3, O4,O5;
    public GameObject ball;
    public ballController BC;
    public PlayerController TC1, TC2, TC3, TC4, TC5,OC1, OC2, OC3, OC4, OC5;
    public int currentHoldBallPlayer;
    int totalstep;

    int timer;
    public int currentstep;
    //public NewOPlayerController OC1, OC2, OC3, OC4;
    void Start()
    {
        //NewTrajectoryManager = GameObject.FindObjectOfType<NewTrajectoryManager>();
        TC1 = T1.GetComponent<PlayerController>();
        TC2 = T2.GetComponent<PlayerController>();
        TC3 = T3.GetComponent<PlayerController>();
        TC4 = T4.GetComponent<PlayerController>();
        TC5 = T5.GetComponent<PlayerController>();

        OC1 = O1.GetComponent<PlayerController>();
        OC2 = O2.GetComponent<PlayerController>();
        OC3 = O3.GetComponent<PlayerController>();
        OC4 = O4.GetComponent<PlayerController>();
        OC5 = O5.GetComponent<PlayerController>();
        BC = ball.GetComponent<ballController>();
        Init_Task();
        taskIsStart = false;
        //read task data
    }

    // Update is called once per frame
    public void Init_Task(){
        TC1.Init_Player();
        TC2.Init_Player();
        TC3.Init_Player();
        TC4.Init_Player();
        TC5.Init_Player();
        OC1.Init_Player();
        OC2.Init_Player();
        OC3.Init_Player();
        OC4.Init_Player();
        OC5.Init_Player();
        totalstep = NewTrajectoryManager.TotalStep;
        currentstep = 0;
    }


    void FixedUpdate(){
        if(totalstep !=0 && taskIsStart){
            if(timer == 1){
                if(currentstep <= totalstep-1){
                    BC.MoveOn_NextPos(currentstep,0);
                    TC1.MoveOn_NextPos(currentstep,1);
                    TC2.MoveOn_NextPos(currentstep,2);
                    TC3.MoveOn_NextPos(currentstep,3);
                    TC4.MoveOn_NextPos(currentstep,4);
                    TC5.MoveOn_NextPos(currentstep,5);
                    OC1.MoveOn_NextPos(currentstep,6);
                    OC2.MoveOn_NextPos(currentstep,7);
                    OC3.MoveOn_NextPos(currentstep,8);
                    OC4.MoveOn_NextPos(currentstep,9);
                    OC5.MoveOn_NextPos(currentstep,10);
                    currentstep += 1;
                }
                timer = 0;
            }else{
                timer += 1;
            }
        }else{
            totalstep = NewTrajectoryManager.TotalStep;
        }
    }
}
