﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    // Start is called before the first frame update
    public NewTaskManager NewTaskManager;
    public bboxManager bboxManager;

    void Start()
    {
        NewTaskManager = GameObject.FindObjectOfType<NewTaskManager>();
        bboxManager = GameObject.FindObjectOfType<bboxManager>();
    }
    public void start_play(){
        NewTaskManager.taskIsStart = true;
        NewTaskManager.currentstep = 0;
        //bboxManager.start_record_bbox = true;
    }

    public void stop_play(){
        NewTaskManager.taskIsStart = false;
        NewTaskManager.currentstep = 0;
        //bboxManager.start_record_bbox = false;
    }

}
