﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*

This script switch between PTZ Cameras.
There are two modes:
    1. PTZ camera alone on one side
    2. Four PTZ cameras with wide camera input
Thus, except for switching cameras, it is also needed to take care of different modes. Don't mix them.

*/


public class MultiCameraController : MonoBehaviour
{
    public Camera LowPTZCamera;
    public Camera HighPTZCamera;
    public Camera WideCamera;
    public Camera SideCamera_lefttop;
    public Camera SideCamera_leftbottom;
    public Camera SideCamera_righttop;
    public Camera SideCamera_rightbottom;

    public Camera currentCamera;
    public Scrollbar scrollbar;
    int mode = 1;

    void Awake()
    {
        //currentCamera = SideCamera_rightbottom;
        currentCamera = LowPTZCamera;
        // LowPTZCamera.enabled = true;
        //LowPTZCamera.enabled = false;
        HighPTZCamera.enabled = false;
        SideCamera_lefttop.enabled = false;
        SideCamera_leftbottom.enabled = false;
        SideCamera_righttop.enabled = false;
        SideCamera_rightbottom.enabled = false;
        //SideCamera_rightbottom.enabled = true;

        // TO FIX
        // scrollbar = GameObject.Find("[UI Panel]/Mode_Switch").GetComponent<Scrollbar>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.anyKeyDown){
            DisableCameras();
            if(mode == 1){
                if(Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1)){
                    currentCamera = LowPTZCamera;
                }
                else if(Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2)){
                    currentCamera = HighPTZCamera;
                }
            }
            else if(mode == 2){
                if(Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3)){
                    currentCamera = SideCamera_lefttop;
                }
                else if(Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Keypad4)){
                    currentCamera = SideCamera_leftbottom;
                }
                else if(Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Keypad5)){
                    currentCamera = SideCamera_righttop;
                }
                else if(Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Keypad6)){
                    currentCamera = SideCamera_rightbottom;
                }
            }
            currentCamera.enabled = true;
        }
        
    }

    void DisableCameras(){
        LowPTZCamera.enabled = false;
        HighPTZCamera.enabled = false;
        SideCamera_lefttop.enabled = false;
        SideCamera_leftbottom.enabled = false;
        SideCamera_righttop.enabled = false;
        SideCamera_rightbottom.enabled = false;
    }

    public void GetValue(){
        if(scrollbar.value == 0.0f){
            mode = 1;
        }
        else if(scrollbar.value == 1.0f){
            mode = 2;
        }
    }
}
