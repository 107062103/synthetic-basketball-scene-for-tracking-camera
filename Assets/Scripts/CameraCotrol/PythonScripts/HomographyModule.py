import cv2
import os
import numpy as np
from datetime import datetime
import math

HIGHT = 450
WIDTH = 800
FOV = 82.68758

# ----2d coordinate of court plane----
# points_2d_all[0:4]   : court edge points
# points_2d_all[4:7]   : left penalty points
# points_2d_all[7:11]  : right penalty points
# points_2d_all[12:13] : middle point
# points_2d_camera     : current camera position in world coordinate
points_2d_camera = np.array([12, 0])
points_2d_all = np.array([[7.5, -14], [-7.5,-14], [-7.5, 14], [7.5,14], [2.4, -14], [-2.4,-14], [-2.4, -8.21], [2.4, -8.21], [2.4, 8.21], [ -2.4, 8.21], [-2.4, 14], [2.4, 14],[7.5, 0], [-7.5, 0]])

# 3d version court points for projection pusrpose
points_3d_camera = np.array([12,2.04, 0])
points_3d_all = np.array([[7.5, 0,-14], [-7.5,0,-14], [-7.5,0, 14], [7.5,0,14], [2.4,0, -14], [-2.4,0,-14], [-2.4,0, -8.21], [2.4, 0, -8.21], [2.4,0,  8.21], [ -2.4,0, 8.21], [-2.4, 0,14], [2.4,0, 14],[7.5,0, 0], [-7.5,0, 0]])


class HomographyModule:
    def __init__(self):
        self.focal_length = WIDTH / (2 * math.tan(math.radians(FOV / 2)))
        self.camera_matrix = np.array([ [self.focal_length, 0, WIDTH/2],[0, self.focal_length, HIGHT/2],[0, 0, 1]])
        self.camera_H = np.zeros((3,3))
        self.rvec = np.zeros((1,3))
        self.tvec = np.zeros((1,3))
    
    def initial_camera_intrinsic_matrix(self, initial_corners):
        initial_corners_bound = []
        compare_list = []
        for i in range(len(initial_corners)):
            if(initial_corners[i][0] > 0 and initial_corners[i][0] < 800 and initial_corners[i][1] > 0 and initial_corners[i][1] < 450):
                initial_corners_bound.append(initial_corners[i])
                compare_list.append(points_3d_all[i]-points_3d_camera)
        initial_corners_bound = np.array(initial_corners_bound)
        compare_list = np.array(compare_list)
        _, self.rvec, self.tvec =  cv2.solvePnP(compare_list, initial_corners_bound, self.camera_matrix, None)

    def initial_homography_matrix(self,initial_corners):
        initial_corners_bound = []
        compare_list = []
        for i in range(len(initial_corners)):
            if(initial_corners[i][0] > 0 and initial_corners[i][0] < 800 and initial_corners[i][1] > 0 and initial_corners[i][1] < 450):
                initial_corners_bound.append(initial_corners[i])
                compare_list.append(points_2d_all[i]-points_2d_camera)
        initial_corners_bound = np.array(initial_corners_bound)
        compare_list = np.array(compare_list)
        self.camera_H, _ = cv2.findHomography(initial_corners_bound, compare_list)

    def update_edge_points(self, new_rvec):
        new_2d, _ = cv2.projectPoints(points_3d_all-points_3d_camera, new_rvec, self.tvec, self.camera_matrix, None)
        preproccess_new_2d = []
        for i in range(len(new_2d)):
            preproccess_new_2d.append([new_2d[i][0][0], new_2d[i][0][1]])
        self.camera_H, _ = cv2.findHomography(np.array(preproccess_new_2d), points_2d_all-points_2d_camera)
        self.rvec = new_rvec
        return new_2d

    def calculate_all_bbox_position(self, bbox_list):
        position_list = []
        for i in range(len(bbox_list)-1):
            x = int(bbox_list[i+1][0]+float(bbox_list[i+1][2])/2)
            y = int(450-bbox_list[i+1][1]+float(bbox_list[i+1][3])/2)
            homogeneous_point = np.array([x,y, 1]).reshape(3, 1)
            plane_position_homogeneous = np.dot(self.camera_H, homogeneous_point)
            plane_position = plane_position_homogeneous / plane_position_homogeneous[2]
            plane_x, plane_y = plane_position[0, 0], plane_position[1, 0]
            position_list.append([plane_x, plane_y])
        return position_list
        

