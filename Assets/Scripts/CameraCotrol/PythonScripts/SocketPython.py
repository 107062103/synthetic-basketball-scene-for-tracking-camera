import socket
import cv2
import os
import numpy as np
from datetime import datetime
from HomographyModule import HomographyModule
import argparse
import random
import math
from scipy.spatial.transform import Rotation   

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('localhost', 8000)  # Use the desired IP address and Port number
server_socket.bind(server_address)

server_socket.listen(1)

connection, client_address = server_socket.accept()
start = False

# Image resolution
RESIZE_WIDTH = 800
RESIZE_HEIGHT = 450

# Size of center region
CENTER_MIN_X = 350 # 300
CENTER_MAX_X = 450 # 500
CENTER_MIN_Y = 185
CENTER_MAX_Y = 275
# Center_PT = (350,450,185,275)
BOUNDARY = 20

time = datetime.now()
time_to_filename = time.strftime("%Y-%m-%d_%H-%M-%S")
# txt_path = "Results/cmd.txt"

img_cnt = 0
img_list = []
bbox_list = []
cmd_list = []
court_pt_list = []
calculated_court_pt = []
bbox_2d_list = []
homography_video_list = []
fps = 0
new_edge_list  = []

# center
os.makedirs("Results/", exist_ok=True)
os.makedirs("Results/Command Record/", exist_ok=True)

parser = argparse.ArgumentParser()
parser.add_argument('--randomly_lose_track', '-r', action='store_true')
parser.add_argument('--lose_num', '-n', type=int, default=0)


def cut_bbox(bbox_string):
    bbox_tmp_list = []
    bbox_tmp = bbox_string.split("/")
    for item in bbox_tmp:
        obj_list = []
        obj_list = item.split(",")
        float_obj_list = [float(i) for i in obj_list]
        bbox_tmp_list.append(float_obj_list)
    return bbox_tmp_list


if __name__ == "__main__":

    homographyModule = HomographyModule()
    initial_camera_parameter = False

    while True:
        # A starting message
        if not start:
            msg = connection.recv(1024)
            if msg:
                print("Received: ", msg.decode('utf-8'))
                response = "Received, sending command..."
                connection.sendall(response.encode('utf-8'))
            start = True
            
        Bytes = connection.recv(1024)
        check = Bytes.decode('utf-8', errors='ignore')

        # finish sending, trying to save them as a video
        if check == "End":
            response = "Server got it!"
            connection.sendall(response.encode('utf-8'))
            fps = connection.recv(4)
            if fps:
                fps = int.from_bytes(fps, byteorder='little')
                print("frame count: ", len(img_list), "video avg fps: ", fps)
                response = "Server received average fps = " + str(fps)
                connection.sendall(response.encode('utf-8'))
                court_line_list = []
                time = datetime.now()
                time_to_filename = time.strftime("%Y-%m-%d_%H-%M-%S")
                out_video = cv2.VideoWriter(f"Results/{time_to_filename}_ball.avi", cv2.VideoWriter_fourcc(*'MJPG'), fps, (RESIZE_WIDTH, RESIZE_HEIGHT))

                #draw lines on video
                for i in range(len(img_list)):
                    for j in range(len(bbox_list[i])):
                        if(j == 0):
                            cv2.rectangle(img_list[i], (int(bbox_list[i][j][0]-(bbox_list[i][j][2])/2), 450-int(bbox_list[i][j][1]-(bbox_list[i][j][3])/2)), (int(bbox_list[i][j][0]+(bbox_list[i][j][2]/2)), 450-int(bbox_list[i][j][1]+(bbox_list[i][j][3])/2)), (0, 255, 0), 1)
                        else:
                            cv2.rectangle(img_list[i], (int(bbox_list[i][j][0]-(bbox_list[i][j][2])/2), 450-int(bbox_list[i][j][1]-(bbox_list[i][j][3])/2)), (int(bbox_list[i][j][0]+(bbox_list[i][j][2]/2)), 450-int(bbox_list[i][j][1]+(bbox_list[i][j][3])/2)), (255, 0, 0), 1)
                        
                        # ground truth lines
                        # edge_pts = np.array([[int(court_pt_list[i][0][0]),450-int(court_pt_list[i][0][1])],[int(court_pt_list[i][1][0]),450-int(court_pt_list[i][1][1])],[int(court_pt_list[i][2][0]),450-int(court_pt_list[i][2][1])],[int(court_pt_list[i][3][0]),450-int(court_pt_list[i][3][1])]], np.int32).reshape((-1,1,2))
                        # cv2.polylines(img_list[i],[edge_pts],True,(255,255,0),1)
                        # penalty_l_pts = np.array([[int(court_pt_list[i][4][0]),450-int(court_pt_list[i][4][1])],[int(court_pt_list[i][5][0]),450-int(court_pt_list[i][5][1])],[int(court_pt_list[i][6][0]),450-int(court_pt_list[i][6][1])],[int(court_pt_list[i][7][0]),450-int(court_pt_list[i][7][1])]], np.int32).reshape((-1,1,2))
                        # cv2.polylines(img_list[i],[penalty_l_pts],True,(255,255,0),1)
                        # penalty_r_pts = np.array([[int(court_pt_list[i][8][0]),450-int(court_pt_list[i][8][1])],[int(court_pt_list[i][9][0]),450-int(court_pt_list[i][9][1])],[int(court_pt_list[i][10][0]),450-int(court_pt_list[i][10][1])],[int(court_pt_list[i][11][0]),450-int(court_pt_list[i][11][1])]], np.int32).reshape((-1,1,2))
                        # cv2.polylines(img_list[i],[penalty_r_pts],True,(255,255,0),1)
                        # middle_pts = np.array([[int(court_pt_list[i][12][0]),450-int(court_pt_list[i][12][1])],[int(court_pt_list[i][13][0]),450-int(court_pt_list[i][13][1])]], np.int32).reshape((-1,1,2))
                        # cv2.polylines(img_list[i],[middle_pts],True,(255,255,0),1)
                        
                        # draw calculated lines
                        new_edge_pts = np.array([[int(new_edge_list[i][0][0][0]),int(new_edge_list[i][0][0][1])],[int(new_edge_list[i][1][0][0]),int(new_edge_list[i][1][0][1])],[int(new_edge_list[i][2][0][0]),int(new_edge_list[i][2][0][1])],[int(new_edge_list[i][3][0][0]),int(new_edge_list[i][3][0][1])]], np.int32).reshape((-1,1,2))
                        cv2.polylines(img_list[i],[new_edge_pts],True,(255,0,255),1)
                        new_pl_pts = np.array([[int(new_edge_list[i][4][0][0]),int(new_edge_list[i][4][0][1])],[int(new_edge_list[i][5][0][0]),int(new_edge_list[i][5][0][1])],[int(new_edge_list[i][6][0][0]),int(new_edge_list[i][6][0][1])],[int(new_edge_list[i][7][0][0]),int(new_edge_list[i][7][0][1])]], np.int32).reshape((-1,1,2))
                        cv2.polylines(img_list[i],[new_pl_pts],True,(255,0,255),1)
                        new_pr_pts = np.array([[int(new_edge_list[i][8][0][0]),int(new_edge_list[i][8][0][1])],[int(new_edge_list[i][9][0][0]),int(new_edge_list[i][9][0][1])],[int(new_edge_list[i][10][0][0]),int(new_edge_list[i][10][0][1])],[int(new_edge_list[i][11][0][0]),int(new_edge_list[i][11][0][1])]], np.int32).reshape((-1,1,2))
                        cv2.polylines(img_list[i],[new_pr_pts],True,(255,0,255),1)
                        new_mi_pts = np.array([[int(new_edge_list[i][12][0][0]),int(new_edge_list[i][12][0][1])],[int(new_edge_list[i][13][0][0]),int(new_edge_list[i][13][0][1])]], np.int32).reshape((-1,1,2))
                        cv2.polylines(img_list[i],[new_mi_pts],True,(255,0,255),1)

                for img in img_list:
                    out_video.write(img)

                txt_path = f"Results/Command Record/{time_to_filename}_ball_cmd.txt"
                f = open(txt_path, "w")
                f.writelines(cmd_list)

                homography_video = cv2.VideoWriter(f"Results/{time_to_filename}_HOMOGRAPHY.avi", cv2.VideoWriter_fourcc(*'MJPG'), fps, (150, 280))
                # draw homography video
                for i in range(len(bbox_2d_list)):
                    img_tmp = np.zeros((280,150,3), np.uint8)
                    for j in range(len(bbox_2d_list[i])):
                        if(j<5):
                            cv2.circle(img_tmp,(int(bbox_2d_list[i][j][0]+7.5+12)*10,int(bbox_2d_list[i][j][1]+14)*10), 3, (0,255,0), -1)
                        else:
                            cv2.circle(img_tmp,(int(bbox_2d_list[i][j][0]+7.5+12)*10,int(bbox_2d_list[i][j][1]+14)*10), 3, (0,0,255), -1)
                    homography_video_list.append(img_tmp)
                
                for img in homography_video_list:
                    homography_video.write(img)

                img_list = []
                bbox_list = []
                bbox_2d_list = []
                cmd_list = []
                court_line_list = []
                homography_video_list = []
                new_edge_list  = []
                homography_video.release()
                out_video.release()
                f.close()

        # recieve bbox from unity
        elif check == "send bbox":
            data = connection.recv(1024)
            bbox_value = data.decode('utf-8', errors='ignore')
            preproccessed = cut_bbox(bbox_value)

            ball_bbox = preproccessed[0]
            if(ball_bbox[0]<CENTER_MIN_X):
                if(abs(ball_bbox[0] - CENTER_MIN_X) <= BOUNDARY):
                    response = "turn left slow"
                else:
                    response = "turn left"
            elif(ball_bbox[0]>CENTER_MAX_X):
                if(abs(ball_bbox[0] - CENTER_MAX_X) <= BOUNDARY):
                    response = "turn right slow"
                else:
                    response = "turn right"
            else:
                response = "stop"
            cmd_list.append(response + "\n")
            bbox_list.append(cut_bbox(bbox_value))

            if initial_camera_parameter == True:
                bbox_2d_list.append(homographyModule.calculate_all_bbox_position(preproccessed))

            connection.sendall(response.encode('utf-8'))

        elif check == "send court point":
            data = connection.recv(1024)
            court_value = data.decode('utf-8', errors='ignore')
            preproccessed = cut_bbox(court_value)
            court_pt_list.append(preproccessed)

            # use first frame to initial camera parameter
            if initial_camera_parameter == False:
                preproccessed_ct = []
                for i in range(len(preproccessed)):
                    preproccessed_ct.append([float(preproccessed[i][0]), float(450-preproccessed[i][1])])
                homographyModule.initial_homography_matrix(np.array(preproccessed_ct))
                homographyModule.initial_camera_intrinsic_matrix(np.array(preproccessed_ct))
                initial_camera_parameter = True
            response = "Server received court pt"
            connection.sendall(response.encode('utf-8'))

        elif check == "send camera angle":
            data = connection.recv(1024)
            angle_value = data.decode('utf-8', errors='ignore')
            angle_value = angle_value.split(",")
            angle_value = np.array([float(i) for i in angle_value])
            r = Rotation.from_quat(angle_value).inv()
            new_edge = homographyModule.update_edge_points(r.as_rotvec())
            new_edge_list.append(new_edge)
            response = "Server received camera angle"
            connection.sendall(response.encode('utf-8'))

        # sending images
        else:
            length = int.from_bytes(Bytes, byteorder='little')
            print("image byte length: ", length)
            data = connection.recv(length) # data type: <class 'bytes'>
            if data:
                img = np.frombuffer(data, dtype='uint8')
                img = cv2.imdecode(img, cv2.IMREAD_COLOR)
                img_cnt += 1
                img = cv2.resize(img, (RESIZE_WIDTH, RESIZE_HEIGHT))
                img_list.append(img)
                response = "Server received an image."
                connection.sendall(response.encode('utf-8'))
            else:
                break
    connection.close()
    # print(img_cnt)
