import socket
import cv2
import os
import numpy as np
from datetime import datetime
import argparse
import random

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('localhost', 8000)  # Use the desired IP address and Port number
server_socket.bind(server_address)

server_socket.listen(1)

connection, client_address = server_socket.accept()
start = False

# Image resolution
RESIZE_WIDTH = 800
RESIZE_HEIGHT = 450

# Size of center region
CENTER_MIN_X = 350 # 300
CENTER_MAX_X = 450 # 500
CENTER_MIN_Y = 185
CENTER_MAX_Y = 275
# Center_PT = (350,450,185,275)
BOUNDARY = 20

# Commands
# TURN_LEFT = ""
# ...

time = datetime.now()
time_to_filename = time.strftime("%Y-%m-%d_%H-%M-%S")

img_cnt = 0
img_list = []
bbox_list = []
center_list = []
cmd_list = []
fps = 0

os.makedirs("Results/", exist_ok=True)

parser = argparse.ArgumentParser()
parser.add_argument('--randomly_lose_track', '-r', action='store_true')
parser.add_argument('--lose_num', '-n', type=int, default=0)

def cut_bbox(bbox_string):
    bbox_tmp_list = []
    bbox_tmp = bbox_string.split("/")
    for item in bbox_tmp:
        obj_list = []
        obj_list = item.split(",")
        float_obj_list = [float(i) for i in obj_list]
        bbox_tmp_list.append(float_obj_list)
    return bbox_tmp_list


if __name__ == "__main__":
    args = parser.parse_args()
    while True:
        # A starting message
        if not start:
            msg = connection.recv(1024)
            if msg:
                print("Received: ", msg.decode('utf-8'))
                response = "Received, sending command..."
                connection.sendall(response.encode('utf-8'))
            start = True
            
        Bytes = connection.recv(1024)
        check = Bytes.decode('utf-8', errors='ignore')

        # Finish sending, trying to save them as a video
        if check == "End":
            response = "Server got it!"
            connection.sendall(response.encode('utf-8'))
            fps = connection.recv(4)
            if fps:
                fps = int.from_bytes(fps, byteorder='little')
                print("video avg fps: ", fps)
                response = "Server received average fps = " + str(fps)
                connection.sendall(response.encode('utf-8'))
                
                time = datetime.now()
                time_to_filename = time.strftime("%Y-%m-%d_%H-%M-%S")
                out_video = cv2.VideoWriter(f"Results/{time_to_filename}_center.avi", cv2.VideoWriter_fourcc(*'MJPG'), fps, (RESIZE_WIDTH, RESIZE_HEIGHT))
                for i in range(len(img_list)):
                    for j in range(len(bbox_list[i])):
                        if(j == 0):
                            cv2.rectangle(img_list[i], (int(bbox_list[i][j][0]-(bbox_list[i][j][2])/2), 450-int(bbox_list[i][j][1]-(bbox_list[i][j][3])/2)), (int(bbox_list[i][j][0]+(bbox_list[i][j][2]/2)), 450-int(bbox_list[i][j][1]+(bbox_list[i][j][3])/2)), (0, 255, 0), 1)
                        else:
                            cv2.rectangle(img_list[i], (int(bbox_list[i][j][0]-(bbox_list[i][j][2])/2), 450-int(bbox_list[i][j][1]-(bbox_list[i][j][3])/2)), (int(bbox_list[i][j][0]+(bbox_list[i][j][2]/2)), 450-int(bbox_list[i][j][1]+(bbox_list[i][j][3])/2)), (255, 0, 0), 1)
                        # draw the center rectangle
                        cv2.rectangle(img_list[i], (CENTER_MIN_X, CENTER_MIN_Y), (CENTER_MAX_X, CENTER_MAX_Y), (0, 0, 255),1)
                        cv2.circle(img_list[i], (center_list[i][0], 450-center_list[i][1]), 5, (0,255,0), -1)
                    out_video.write(img_list[i])
                for img in img_list:
                    out_video.write(img)

                txt_path = f"Results/Command Record/{time_to_filename}_center_cmd.txt"
                f = open(txt_path, "w")
                f.writelines(cmd_list)

                img_list = []
                bbox_list = []
                center_list = []
                cmd_list = []
                out_video.release()
                f.close()

        # Recieve bbox from Unity
        elif check == "send bbox":
            data = connection.recv(1024)
            bbox_value = data.decode('utf-8', errors='ignore')
            preproccessed = cut_bbox(bbox_value)

            player_cenX = 0.0
            player_cenY = 0.0
            total_count = 0

            # Randomly lose track
            if args.randomly_lose_track:
                # num = args.lose_num
                lose_idx = random.sample(range(1, 10), args.lose_num)
                for idx in lose_idx:
                    preproccessed[idx][0] = -10000.0
                    preproccessed[idx][1] = -10000.0
                    # print("lose track of idx: ", idx)

            for i in range(1, 11):
                # If the bbox is out of the camera view
                if preproccessed[i][0] < 0:
                    # print(i, " is not in sight.")
                    continue
                total_count += 1
                player_cenX += preproccessed[i][0]
                player_cenY += preproccessed[i][1]

            # No player is in sight 
            # (Further more rules to determine the pan (e.g. previous cmd, previous direction))
            # Now just set the center as the center of the view
            if total_count == 0:
                player_cenX = 400
                player_cenY = 225
            else:
                player_cenX /= total_count
                player_cenY /= total_count
            center_list.append((int(player_cenX), int(player_cenY)))

            ball_bbox = preproccessed[0]
            # print("ball_bbox: ", ball_bbox)
            if(player_cenX<CENTER_MIN_X):
                if(abs(player_cenX - CENTER_MIN_X) <= BOUNDARY):
                    response = "turn left slow"
                else:
                    response = "turn left"
            elif(player_cenX>CENTER_MAX_X):
                if(abs(player_cenX - CENTER_MAX_X) <= BOUNDARY):
                    response = "turn right slow"
                else:
                    response = "turn right"
            else:
                response = "stop"
            cmd_list.append(response + ", player count = " + str(total_count) + "\n")

            # bbox_list.append(cut_bbox(bbox_value))
            bbox_list.append(preproccessed)
            connection.sendall(response.encode('utf-8'))

        # Receive images from python
        else:
            length = int.from_bytes(Bytes, byteorder='little')
            print("image byte length: ", length)
            data = connection.recv(length) # data type: <class 'bytes'>
            if data:
                img = np.frombuffer(data, dtype='uint8')
                img = cv2.imdecode(img, cv2.IMREAD_COLOR)
                img_cnt += 1
                img = cv2.resize(img, (RESIZE_WIDTH, RESIZE_HEIGHT))
                img_list.append(img)
                response = "Server received an image."
                connection.sendall(response.encode('utf-8'))
            else:
                break
    connection.close()