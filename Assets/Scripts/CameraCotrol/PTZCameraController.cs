﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PTZCameraController : MonoBehaviour
{
    public Vector3 currentRotation;
    public int rotateDir = 0; // 0 for default, 1 for left, 2 for right
    public bool isContinuous = false;

    void Start()
    {
        currentRotation = gameObject.transform.rotation.eulerAngles;
    }

    public void OnPythonControl(string rotatemode){
        if(rotatemode == "turn left"){ //left
            gameObject.transform.Rotate(-Vector3.up * Time.deltaTime * 20);
            Debug.Log("[CMD] turn left.");
        }else if(rotatemode == "turn right"){ //right
            gameObject.transform.Rotate(Vector3.up * Time.deltaTime * 20);
            Debug.Log("[CMD] turn right.");
        }else if(rotatemode == "turn stop"){ //stop
            gameObject.transform.Rotate(Vector3.up * Time.deltaTime * 0);
            Debug.Log("[CMD] stop.");
        }else if(rotatemode == "turn left slow"){
            gameObject.transform.Rotate(-Vector3.up * Time.deltaTime * 10);
            Debug.Log("[CMD] turn left slowly.");
        }else if(rotatemode == "turn right slow"){
            gameObject.transform.Rotate(Vector3.up * Time.deltaTime * 10);
            Debug.Log("[CMD] turn right slowly.");
        }
    }

    void Update()
    {
        // Left rotation
        if(rotateDir == 1){
            if(isContinuous){
                gameObject.transform.Rotate(-Vector3.up * Time.deltaTime * 10);
            }
            else{
                if(currentRotation.y - gameObject.transform.rotation.eulerAngles.y < 45){
                    gameObject.transform.Rotate(-Vector3.up * Time.deltaTime * 50);
                }
                else{
                    currentRotation = gameObject.transform.rotation.eulerAngles;
                    rotateDir = 0;
                }
            }
            
        }
        // Right rotation
        else if(rotateDir == 2){
            if(isContinuous){
                gameObject.transform.Rotate(Vector3.up * Time.deltaTime * 10);
            }
            else{
                if(gameObject.transform.rotation.eulerAngles.y - currentRotation.y < 45){
                    gameObject.transform.Rotate(Vector3.up * Time.deltaTime * 50);
                }
                else{
                    currentRotation = gameObject.transform.rotation.eulerAngles;
                    rotateDir = 0;
                }
            }
        }
    }
}
