﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    public bool MinimapIsOpen = false;
    public GameObject minimap;
    
    public Button discreteLeft;
    public Button discreteRight;
    public Button continuousLeft;
    public Button continuousRight;
    public Button stopButton;
    public Button sendImageButton;

    public MultiCameraController cameraController;
    public SocketClient socketClient;
    

    Camera currentPTZCamera;
    PTZCameraController PTZController;
    CaptureCameraView captureCamera;

    void Start()
    {
        // discreteLeft.onClick.AddListener(OnClickLeft);
        // discreteRight.onClick.AddListener(OnClickRight);
        // continuousLeft.onClick.AddListener(OnStartRotateLeft);
        // continuousRight.onClick.AddListener(OnStartRotateRight);
        // stopButton.onClick.AddListener(OnClickStop);

        // sendImageButton.onClick.AddListener(OnClickSendingImage);

        currentPTZCamera = cameraController.currentCamera;
        PTZController = currentPTZCamera.gameObject.GetComponent<PTZCameraController>();
        captureCamera = currentPTZCamera.gameObject.GetComponent<CaptureCameraView>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void GetCamera(){
        currentPTZCamera = cameraController.currentCamera;
        PTZController = currentPTZCamera.gameObject.GetComponent<PTZCameraController>();
        captureCamera = currentPTZCamera.gameObject.GetComponent<CaptureCameraView>();
    }


    public void OnClickLeft(){
        Debug.Log("PTZ Camera to left.");
        GetCamera();
        PTZController.isContinuous = false;
        PTZController.rotateDir = 1;
    }

    public void OnClickRight(){
        Debug.Log("PTZ Camera to right.");
        GetCamera();
        PTZController.isContinuous = false;
        PTZController.rotateDir = 2;
        // Just jump to the fixed point, not slowly rotating
        // gameObject.transform.eulerAngles = new Vector3(0, -40, 0);
    }

    public void OnStartRotateLeft(){
        Debug.Log("Start to rotate to left.");
        GetCamera();
        PTZController.isContinuous = true;
        PTZController.rotateDir = 1;
    }

    public void OnStartRotateRight(){
        Debug.Log("Start to rotate to right.");
        GetCamera();
        PTZController.isContinuous = true;
        PTZController.rotateDir = 2;
    }

    public void OnClickStop(){
        GetCamera();
        PTZController.isContinuous = false;
        PTZController.rotateDir = 0;
    }

    public void OnClickMinimap(){
        if(MinimapIsOpen){
            Debug.Log("Turn off the minimap.");
            minimap.SetActive(false);
            MinimapIsOpen = false;
        }
        else{
            Debug.Log("Turn on the minimap.");
            minimap.SetActive(true);
            MinimapIsOpen = true;
        }
    }

    public void OnClickSendingImage(){
        GetCamera();
        if(captureCamera.socketSwitch){
            Debug.Log(captureCamera.img_cnt);
            float avg_fps = captureCamera.img_cnt / captureCamera.timer;
            Debug.Log("time: " + captureCamera.timer +", fps: " + avg_fps);
            captureCamera.timer = 0.0f;
            captureCamera.img_cnt = 0;
            captureCamera.socketSwitch = false;

            socketClient.SendImageFPS(avg_fps);
        }
        else{
            captureCamera.socketSwitch = true;
        }
    }
}
