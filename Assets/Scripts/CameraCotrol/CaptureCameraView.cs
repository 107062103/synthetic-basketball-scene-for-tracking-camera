﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using UnityEditor;
using UnityEditor.Media;

public class CaptureCameraView : MonoBehaviour
{
    public float avg_fps = 0f;
    public bool socketSwitch = false;
    public float timer = 0.0f;
    public int img_cnt = 0;
    bboxManager BboxManager;
    CourtLineManager CourtLineManager;
    public SocketClient socketClient;
    public PTZCameraController ptzCameraController;
    public MultiCameraController cameraController;
    Camera cam;

    void Start()
    {
        BboxManager = GameObject.Find("Bbox Manager").GetComponent<bboxManager>();
        CourtLineManager = GameObject.Find("CourtLineManager").GetComponent<CourtLineManager>();
        cameraController = GameObject.Find("MultiCameraController").GetComponent<MultiCameraController>();
        cam = cameraController.currentCamera;
        ptzCameraController = cam.GetComponent<PTZCameraController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(socketSwitch){
            timer += Time.deltaTime;
        }    

    }
    
    void OnPostRender()
    {
        if(socketSwitch){
            Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
            texture.Apply();
            Byte[] bytes = texture.EncodeToPNG();
            socketClient.SendImageToPython(bytes);
            string command = socketClient.SendBboxToPython(BboxManager.get_all_bbox());
            socketClient.SendCourtPtToPython(CourtLineManager.get_string_courtpoint_position());
            socketClient.SendCameraAngle(cam.transform.localRotation.x.ToString()+","+cam.transform.localRotation.y.ToString()+","+cam.transform.localRotation.z.ToString()+","+cam.transform.localRotation.w.ToString());
            ptzCameraController.OnPythonControl(command);
            img_cnt += 1;
            // System.IO.File.WriteAllBytes("frame.png", bytes);
        }  
    }
}
