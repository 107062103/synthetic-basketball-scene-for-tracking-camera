﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using UnityEditor;
using UnityEditor.Media;

public class SocketClient : MonoBehaviour
{
    public ButtonController buttonController;

    public float avg_fps = 0f;
    public float timer = 0.0f;
    public int img_cnt = 0;
    private TcpClient client;
    NetworkStream stream;

    void Start()
    {
        client = new TcpClient("127.0.0.1", 8000);
        stream = client.GetStream();
        SendStringToPython("Connecting Success!");
    }

    void OnDestroy()
    {
        
        stream.Close();
        client.Close();
    }
    
    void OnApplicationQuit()
    {
        Debug.Log("quit");
    }

    public void SendStringToPython(string message)
    {
        if (client.Connected)
        {
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
            stream.Write(data, 0, data.Length);
            Debug.Log("Sent: " + message);

            data = new Byte[256];
            String responseData = String.Empty;
            Int32 bytes = stream.Read(data, 0, data.Length);
            responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
            Debug.Log("Received: " + responseData);
        }
    }

    public void SendImageToPython(Byte[] img)
    {
        if (client.Connected)
        {
            var lengthBytes = BitConverter.GetBytes(img.Length);
            stream.Write(lengthBytes, 0, lengthBytes.Length);
            stream.Write(img, 0, img.Length);

            Debug.Log("Client is Sending an image");

            Byte[] data;
            data = new Byte[256];
            String responseData;
            Int32 bytes = stream.Read(data, 0, data.Length);
            responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
            Debug.Log("Received: " + responseData);
        }
    }

    public string SendBboxToPython(string bbox)
    {
        if (client.Connected)
        {
            Byte[] msg_data = System.Text.Encoding.ASCII.GetBytes("send bbox");
            Byte[] bbox_data = System.Text.Encoding.ASCII.GetBytes(bbox);
            stream.Write(msg_data, 0, msg_data.Length);
            stream.Write(bbox_data, 0, bbox_data.Length);

            Debug.Log("Sent bbox: " + bbox);

            msg_data = new Byte[256];
            String responseData = String.Empty;
            Int32 bytes = stream.Read(msg_data, 0, msg_data.Length);
            responseData = System.Text.Encoding.ASCII.GetString(msg_data, 0, bytes);
            Debug.Log("Received: " + responseData);
            return responseData;
        }
        return "not conntected";
    }

    public string SendCourtPtToPython(string court_pt)
    {
        if (client.Connected)
        {
            Byte[] msg_data = System.Text.Encoding.ASCII.GetBytes("send court point");
            Byte[] court_pt_data = System.Text.Encoding.ASCII.GetBytes(court_pt);
            stream.Write(msg_data, 0, msg_data.Length);
            stream.Write(court_pt_data, 0, court_pt_data.Length);

            Debug.Log("Sent Court Point: " + court_pt);

            msg_data = new Byte[256];
            String responseData = String.Empty;
            Int32 bytes = stream.Read(msg_data, 0, msg_data.Length);
            responseData = System.Text.Encoding.ASCII.GetString(msg_data, 0, bytes);
            Debug.Log("Received: " + responseData);
            return responseData;
        }
        return "not conntected";
    }

    public string SendCameraAngle(string angle)
    {
        if (client.Connected)
        {
            Byte[] msg_data = System.Text.Encoding.ASCII.GetBytes("send camera angle");
            Byte[] angle_data = System.Text.Encoding.ASCII.GetBytes(angle);
            stream.Write(msg_data, 0, msg_data.Length);
            stream.Write(angle_data, 0, angle_data.Length);

            Debug.Log("Sent Camera Angle: " + angle);

            msg_data = new Byte[256];
            String responseData = String.Empty;
            Int32 bytes = stream.Read(msg_data, 0, msg_data.Length);
            responseData = System.Text.Encoding.ASCII.GetString(msg_data, 0, bytes);
            Debug.Log("Received: " + responseData);
            return responseData;
        }
        return "not conntected";
    }

    public void SendImageFPS(float avg){
        SendStringToPython("End");
        avg_fps = (int)avg;

        if (client.Connected)
        {
            var fpsBytes = BitConverter.GetBytes((int)avg_fps);
            stream.Write(fpsBytes, 0, fpsBytes.Length);
            Debug.Log("Sent: average fps = " + avg_fps);

            Byte[] data = new Byte[256];
            String responseData = String.Empty;
            Int32 bytes = stream.Read(data, 0, data.Length);
            responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
            Debug.Log("Received: " + responseData);
        }
    }
}
