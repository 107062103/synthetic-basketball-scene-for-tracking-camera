﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class playerBBoxController : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform  up;
    public Transform down;
    public Transform leftHand;
    public Transform rightHand;
    public Transform leftShoulder;
    public Transform rightShoulder;
    public Transform leftFoot;
    public Transform rightFoot;
    public Transform frontChest;
    public Transform back;
    public Transform middle;
    public Camera cam;
    public Image cover;

    public MultiCameraController cameraController;

    void Start()
    {
        up = transform.Find("Armature/Root/Hips/Spine/Spine1/Neck/Head/up");
        down = transform.Find("down");
        leftHand = transform.Find("Armature/Root/Hips/Spine/Spine1/LeftShoulder/LeftArm/LeftForeArm/LeftHand/LeftHandThumb1/left hand");
        rightHand = transform.Find("Armature/Root/Hips/Spine/Spine1/RightShoulder/RightArm/RightForeArm/RightHand/RightHandThumb1/right hand");
        leftShoulder = transform.Find("Armature/Root/Hips/Spine/Spine1/LeftShoulder/LeftArm/left shoulder");
        rightShoulder = transform.Find("Armature/Root/Hips/Spine/Spine1/RightShoulder/RightArm/right shoulder");
        leftFoot = transform.Find("Armature/Root/Hips/LeftUpLeg/LeftLeg/LeftFoot/LeftToeBase/left foot");
        rightFoot = transform.Find("Armature/Root/Hips/RightUpLeg/RightLeg/RightFoot/RightToeBase/right foot");
        frontChest = transform.Find("front chest");
        back = transform.Find("back");
        // cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        cam = cameraController.currentCamera;
    }

    public (Vector2,Vector2) get_bbox(){
        float up_pos = cam.WorldToScreenPoint(up.position).y;
        float down_pos = cam.WorldToScreenPoint(down.position).y;
        float left_foot_pos_y = cam.WorldToScreenPoint(leftFoot.position).y;
        float right_foot_pos_y = cam.WorldToScreenPoint(rightFoot.position).y;

        float[] y_axis = {down_pos,left_foot_pos_y,right_foot_pos_y};

        float left_hand_pos = cam.WorldToScreenPoint(leftHand.position).x;
        float right_hand_pos = cam.WorldToScreenPoint(rightHand.position).x;
        float left_shoulder_pos = cam.WorldToScreenPoint(leftShoulder.position).x;
        float right_shoulder_pos = cam.WorldToScreenPoint(rightShoulder.position).x;
        float left_foot_pos_x = cam.WorldToScreenPoint(leftFoot.position).x;
        float right_foot_pos_x = cam.WorldToScreenPoint(rightFoot.position).x;
        float frontchest_pos = cam.WorldToScreenPoint(frontChest.position).x;
        float back_pos = cam.WorldToScreenPoint(back.position).x;
        float[] x_axis = {left_hand_pos,right_hand_pos,left_shoulder_pos,right_shoulder_pos,left_foot_pos_x,right_foot_pos_x,frontchest_pos,back_pos};
    

        Vector2 offset = new Vector2(Mathf.Max(x_axis) - Mathf.Min(x_axis),up_pos - Mathf.Min(y_axis));
        Vector2 centor_pt = new Vector2(Mathf.Min(x_axis)+((Mathf.Max(x_axis) - Mathf.Min(x_axis))/2), Mathf.Min(y_axis)+(up_pos - Mathf.Min(y_axis))/2);

        return (centor_pt,offset);
    }

    public string get_string_bbox(){
        Vector2 offset ;
        Vector2 centor_pt ;
        (offset, centor_pt) = get_bbox();
        string offset_s = offset.ToString();
        string centor_pt_s = centor_pt.ToString();
        offset_s = offset_s.Replace("(","").Replace(")","").Replace(" ","");
        centor_pt_s = centor_pt_s.Replace("(","").Replace(")","").Replace(" ","");
        string bbox_tmp = offset_s+ ',' + centor_pt_s;
        return bbox_tmp;
    }

    public Vector2 get_width_height(){
        float up_pos = cam.WorldToScreenPoint(up.position).y;
        float down_pos = cam.WorldToScreenPoint(down.position).y;
        float left_hand_pos = cam.WorldToScreenPoint(leftHand.position).x;
        float right_hand_pos = cam.WorldToScreenPoint(rightHand.position).x;
        float left_shoulder_pos = cam.WorldToScreenPoint(leftShoulder.position).x;
        float right_shoulder_pos = cam.WorldToScreenPoint(rightShoulder.position).x;
        float left_foot_pos = cam.WorldToScreenPoint(leftFoot.position).x;
        float right_foot_pos = cam.WorldToScreenPoint(rightFoot.position).x;
        float frontchest_pos = cam.WorldToScreenPoint(frontChest.position).x;
        float back_pos = cam.WorldToScreenPoint(back.position).x;
        float[] x_axis = {left_hand_pos,right_hand_pos,left_shoulder_pos,right_shoulder_pos,left_foot_pos,right_foot_pos,frontchest_pos,back_pos};
        float[] y_axis = {down_pos,cam.WorldToScreenPoint(leftFoot.position).y,cam.WorldToScreenPoint(rightFoot.position).y};
        float width = Mathf.Max(x_axis) - Mathf.Min(x_axis);
        float height = up_pos - Mathf.Min(y_axis);
        return new Vector2(width,height);
    }
    // Update is called once per frame
    void Update()
    {
        cam = cameraController.currentCamera;
        // Center of the bbox, size of the bbox
        (cover.transform.position,cover.rectTransform.sizeDelta) = get_bbox();

        // If the center of a player is outside the screen, set its position and sizeDelta to 0 (to not send its bbox to the server)
        if(cover.transform.position.x < 0 || cover.transform.position.x > Screen.width){
            // Debug.Log(gameObject.name + " is not in camera view.");
            cover.transform.position = new Vector2(-10000f, -10000f);
            cover.rectTransform.sizeDelta = new Vector2(-10000f, -10000f);
        }
    }
}
