﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
public class bboxManager : MonoBehaviour
{
    // Start is called before the first frame update
    
    //public Text upText;
    public GameObject T1, T2, T3, T4, T5, O1, O2, O3, O4, O5, Ball;
    public playerBBoxController TB1, TB2, TB3, TB4, TB5, OB1, OB2, OB3, OB4, OB5;
    public ballController BB;
    private string BBOX ;
    public bool start_record_bbox = false;

    void Start()
    {
        TB1 = T1.GetComponent<playerBBoxController>();
        TB2 = T2.GetComponent<playerBBoxController>();
        TB3 = T3.GetComponent<playerBBoxController>();
        TB4 = T4.GetComponent<playerBBoxController>();
        TB5 = T5.GetComponent<playerBBoxController>();
        OB1 = O1.GetComponent<playerBBoxController>();
        OB2 = O2.GetComponent<playerBBoxController>();
        OB3 = O3.GetComponent<playerBBoxController>();
        OB4 = O4.GetComponent<playerBBoxController>();
        OB5 = O5.GetComponent<playerBBoxController>();
        BB = Ball.GetComponent<ballController>();
    }

    public string get_all_bbox(){
        string bbox_tmp = BB.get_string_bbox()+'/'+TB1.get_string_bbox() +'/'+ TB2.get_string_bbox() +'/'+ TB3.get_string_bbox() +'/'+ TB4.get_string_bbox() +'/'+ TB5.get_string_bbox() +'/'+ OB1.get_string_bbox() +'/'+ OB2.get_string_bbox() +'/'+ OB3.get_string_bbox() +'/'+ OB4.get_string_bbox() +'/'+ OB5.get_string_bbox();
        return bbox_tmp;
    }

    void write_file(){
        string path = "Assets/Resources/bbox.txt";
        if (!File.Exists(path)) {
            File.WriteAllText(path, "");
        }
        File.AppendAllText(path, BBOX);
    }
    // Update is called once per frame
    void Update()
    {
        //get_all_bbox();
        if(start_record_bbox){
            BBOX = BBOX + get_all_bbox() + '\n';
        }else{
            write_file();
        }
    }
}
