﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewTPlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    private string playerName;
    private int playerID;
    private NewTrajectoryManager NewTrajectoryManager;
    private Animator Animator;

    void Awake()
    {
        NewTrajectoryManager = GameObject.FindObjectOfType<NewTrajectoryManager>();
        playerName = gameObject.name;
        //playerID = int.Parse(playerName.Replace("T", ""));
        print(playerName);
        Animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    public void Init_Player()
    {
        Animator.SetFloat("_move", 0.0f);
    }

    public void MoveOn_NextPos(int step,int playerID)
    {
        Vector3 prepos, pos;
        //Vector2 pos;
        pos = NewTrajectoryManager.GetPosition(step, playerID);
        
        if(step != 0)
        {
            prepos = NewTrajectoryManager.GetPosition(step - 1, playerID);
            Control_Animation(prepos, pos);
        }

        gameObject.transform.LookAt(new Vector3(0.28f*(pos[0]-25), 0, 0.28f*(pos[1]-50)));
        gameObject.transform.position = new Vector3(0.28f*(pos[0]-25), 0, 0.28f*(pos[1]-50));
    }

    private void Control_Animation(Vector3 prepos, Vector3 pos)
    {
        Animator.speed = 0.75f;
        if(Mathf.Abs(prepos[0] - pos[0]) < 0.08f && Mathf.Abs(prepos[1] - pos[1]) < 0.08f){
            Animator.SetFloat("_move", 0.0f);
        }else{
            Animator.SetFloat("_move", 2.0f);
        }
    }

    void Update()
    {
        
    }
}
