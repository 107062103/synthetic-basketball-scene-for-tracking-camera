﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    private string playerName;
    private int playerID;
    private NewTrajectoryManager NewTrajectoryManager;
    private Animator Animator;
    private int dribbleIndex;
    private bool holdball = false;
    public GameObject ball;

    void Awake()
    {
        NewTrajectoryManager = GameObject.FindObjectOfType<NewTrajectoryManager>();
        playerName = gameObject.name;
        Animator = gameObject.GetComponent<Animator>();
        dribbleIndex = Animator.GetLayerIndex("Dribble");
        holdball = false;
    }
    void start(){
  
    }
    // Update is called once per frame
    public void Init_Player()
    {
        Animator.SetTrigger("tacticStart");
        Animator.SetTrigger("ResetDribbling");
        Animator.SetLayerWeight(dribbleIndex, 1.0f);
    }

    public void MoveOn_NextPos(int step,int playerID)
    {
        Vector3 prepos, pos;
        //Vector2 pos;
        pos = NewTrajectoryManager.GetPosition(step, playerID);
        
        if(step != 0)
        {
            prepos = NewTrajectoryManager.GetPosition(step - 1, playerID);
            Control_Animation(prepos, pos);
            if(Mathf.Abs(prepos[0] - pos[0]) > 0.08f || Mathf.Abs(prepos[1] - pos[1]) > 0.08f){
                gameObject.transform.LookAt(new Vector3(0.28f*(pos[0]-25), 0, 0.28f*(pos[1]-50)));
            }else{
                //gameObject.transform.LookAt(new Vector3(ball.transform.position.x-transform.position.x, 0, ball.transform.position.z-transform.position.z));
            }
        }
        //gameObject.transform.LookAt(new Vector3(0.28f*(pos[0]-25), 0, 0.28f*(pos[1]-50)));
        gameObject.transform.position = new Vector3(0.28f*(pos[0]-25), 0, 0.28f*(pos[1]-50));
    }

    private void Control_Animation(Vector3 prepos, Vector3 pos)
    {
        Animator.speed = 0.9f;
        if(Mathf.Abs(prepos[0] - pos[0]) < 0.08f && Mathf.Abs(prepos[1] - pos[1]) < 0.08f){
            Animator.SetFloat("speed", 0.0f);
        }else{
            Animator.SetFloat("speed", 2.0f);
            //Animator.SetBool("isDribbling", true);
        }
    }

    void Update()
    {
        
    }
}
