﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NewTrajectoryManager : MonoBehaviour
{
    // Start is called before the first frame 
    public TextAsset trajectory;

    public List<List<Vector3>> Trajectory = new List<List<Vector3>>();
    public int TotalStep;
    // court size = (100,50)
    void Start()
    {
        //read trajactory data
        trajectory = Resources.Load<TextAsset>("SportVUJson/0021500001");
        string[] content = trajectory.text.Split('\n');
        List<Vector3> _trajectory = new List<Vector3>();
        foreach (var item in content)
        {
            string[] data = item.Split('/');
            //print(data.Length);
            _trajectory = new List<Vector3>();
            for (int i = 0 ; i < 11 ; i++){
                float y = 0.0f;
                if(i == 0){
                    //print(data[i].Split(','));
                    y = float.Parse(data[i].Split(',')[2]);
                }
                float x = float.Parse(data[i].Split(',')[0]);
                float z = float.Parse(data[i].Split(',')[1]);
                //print(x);
                Vector3 pos = new Vector3(z,x,y);
                _trajectory.Add(pos);
            }
            Trajectory.Add(_trajectory);
        }
        Trajectory.Add(_trajectory);
        TotalStep = Trajectory.Count;
    }

    public Vector3 GetPosition(int step,int playerID)
    {
        // playerid = 0 --> ball, playerid = 1 --> user
        Vector3 result = Trajectory[step][playerID];
        return result;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
