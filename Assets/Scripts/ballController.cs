﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ballController : MonoBehaviour
{
    // Start is called before the first frame update
    private NewTrajectoryManager NewTrajectoryManager;
    private float y_axis = 1.14f;
    private bool bounce = true;
    public Image cover;
    public Transform  top;
    public Transform down;
    public Transform center;
    public Camera cam;
    public MultiCameraController cameraController;

    void Awake()
    {
        // top = transform.Find("top");
        // down = transform.Find("down");
        NewTrajectoryManager = GameObject.FindObjectOfType<NewTrajectoryManager>();
        cam = cameraController.currentCamera;
    }

    // Update is called once per frame

    public void MoveOn_NextPos(int step,int playerID)
    {
        Vector3 pos;

        pos = NewTrajectoryManager.GetPosition(step, playerID);
        
        gameObject.transform.LookAt(new Vector3(0.28f*(pos[0]-25), y_axis, 0.28f*(pos[1]-50)));
        gameObject.transform.position = new Vector3(0.28f*(pos[0]-25), y_axis, 0.28f*(pos[1]-50));
        if(bounce){
            if(y_axis > 0.115f){
                y_axis -= 0.1f;
            }else{
                bounce = false;
            }
        }else{
            if(y_axis < 1.14f){
                y_axis += 0.1f;
            }else{
                bounce = true;
            }
        }
    }

    public (Vector2,Vector2) get_bbox(){
        float top_pos = cam.WorldToScreenPoint(top.position).y;
        float down_pos = cam.WorldToScreenPoint(down.position).y;
        Vector2 centor_pt = new Vector2(cam.WorldToScreenPoint(center.position).x,cam.WorldToScreenPoint(center.position).y);
        //print(top_pos-down_pos);
        Vector2 offset = new Vector2(Mathf.Max(top_pos,down_pos)-Mathf.Min(top_pos,down_pos),Mathf.Max(top_pos,down_pos)-Mathf.Min(top_pos,down_pos));
        return (centor_pt,offset);
    }

    public string get_string_bbox(){
        Vector2 offset ;
        Vector2 centor_pt ;
        (offset, centor_pt) = get_bbox();
        string offset_s = offset.ToString();
        string centor_pt_s = centor_pt.ToString();
        offset_s = offset_s.Replace("(","").Replace(")","").Replace(" ","");
        centor_pt_s = centor_pt_s.Replace("(","").Replace(")","").Replace(" ","");
        string bbox_tmp = offset_s+ ',' + centor_pt_s;
        return bbox_tmp;
    }

    void Update()
    {
        cam = cameraController.currentCamera;
        (cover.transform.position,cover.rectTransform.sizeDelta) = get_bbox();
    }

    private void OnTriggerEnter(Collider other)
    {
        print(other.gameObject.name);
    }
}
